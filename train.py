#!/usr/bin/env python3
"""
Create a classifier from a provided model and training set, and write the
optimised classifier to disk for use in prediction scripts.

The classifier will take word-embeddings from a transformer model and use
these to construct and optimise a Linear SVC.

This is not how you would write a training script for production use.
There's almost no batching amongst a bunch of other concerns. But it works and
avoids the worst of the numpy/pandas confusion for people unfamiliar with them.
"""

import dataclasses
import json
import pickle

from typing import List, Tuple, Iterable

from sentence_transformers import SentenceTransformer

import numpy as np
import torch

import sklearn.svm
import sklearn.pipeline
import sklearn.model_selection

from data import Document, load_document, token_to_word_embedding


@dataclasses.dataclass
class Example:
    """
    Holds a preprocessed text and the electorates that should and should-not
    be predicted for this text.
    """
    doc: Document
    good: List[str] = dataclasses.field(default_factory=list)
    bad: List[str] = dataclasses.field(default_factory=list)


def load_examples(path: str, tf: SentenceTransformer) -> Iterable[Example]:
    """
    Yield an `Example` for every line in the JSON training file.
    """
    with open(path, 'rt') as f:
        data = json.load(f)

    for entry in data:
        doc = Example(
            doc=load_document(tf, entry['text']),
            good=entry.get('good', []),
            bad=entry.get('bad', [])
        )

        assert doc.good or doc.bad
        yield doc


def example_to_pair(ex: Example, tf: SentenceTransformer) -> Iterable[Tuple[torch.Tensor, int]]:
    """
    Yield each of the `embedding, category` pairs for an Example object.
    """
    for accept, keywords in ((1, ex.good), (0, ex.bad)):
        for target in keywords:
            for embedding in token_to_word_embedding(ex.doc, target, tf.tokenizer):
                yield embedding.tolist(), accept


def load_training_data(path: str, model: str) -> Tuple[np.array, List[int]]:
    """
    Return the complete list of values and labels from the training set.
    """
    tf = SentenceTransformer(model)

    examples = list(load_examples(path, tf=tf))
    values, labels = zip(*(
        pair for ex in examples for pair in example_to_pair(ex, tf)
    ))

    return np.array(values), labels


def build_classifier(values: List[np.ndarray], labels: List[int]):
    """
    Build a classifier that will predict the provided labels for each of the
    provided values.

    Depending on the underlying model this may take some time while the
    hyperparameter space is searched.

    LinearSVC is used because it's amongst the most accurate for our data,
    with low predicted variance, and executes/trains pretty cheaply.
    RandomForests and MLP are decent options but look like they might overfit.
    """

    # We don't need a pipeline, but it lets us use GridSearchCV.
    pipeline = sklearn.pipeline.Pipeline([
        # max_iter: Our dataset tends to need more than the default 1_000
        #   iterations to converge. It's worth revisiting if more training
        #   data is collected.
        # class_weight: try to bias the classes so that we avoid
        #   false-positives at the cost of false-negatives.
        ('clf', sklearn.svm.LinearSVC(max_iter=100_000, class_weight={1: 1, 0: 5}))
    ])

    params = {
        'clf__loss': ['hinge', 'squared_hinge'],
        'clf__C': (0.1, 0.3, 0.5, 0.7, 0.9, 0.95, 1, 1.05, 1.1, 1.3, 5, 10),
    },

    clf = sklearn.model_selection.GridSearchCV(
        pipeline,
        params,
        n_jobs=-1
    )
    clf.fit(values, labels)

    # Get an idea for how robust it is
    validation = sklearn.model_selection.cross_val_score(
        clf, values, labels,
        cv=sklearn.model_selection.KFold(),
    )

    print(
        "cross validation:\nmean {mean:.2f}\nstd: {stddev:.2f}\n{params}".format(
            mean=validation.mean(),
            stddev=validation.std(),
            params=clf.best_params_,
        )
    )

    # Log some of the performance statistics for convenience.
    clf.fit(values, labels)
    print(
        sklearn.metrics.classification_report(
            labels,
            clf.predict(values)
        )
    )

    return clf


if __name__ == '__main__':
    import argparse

    def main():
        # all-MiniLM-L6-v2 was chosen entirely because it's the smallest
        # model with reasonable performance.
        #
        # It would be reasonable to assume a larger model would result in
        # higher accuracy at the cost of increase memory consumption.
        # Recommended models can be found at
        #   https://www.sbert.net/docs/pretrained_models.html
        parser = argparse.ArgumentParser()
        parser.add_argument('--input', type=str, required=True, help="The path to the training data file")
        parser.add_argument('--output', type=str, required=True, help="The path to save the final model")
        parser.add_argument(
            '--model',
            type=str,
            default='all-MiniLM-L6-v2',
            help="The transformer model to use for embeddings"
        )
        args = parser.parse_args()

        values, labels = load_training_data(path=args.input, model=args.model)
        clf = build_classifier(values, labels)

        with open(args.output, 'wb') as dst:
            pickle.dump(clf, dst)

    main()

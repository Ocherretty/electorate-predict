# A predictor for electorate names in text

These scripts are one rough approach to predicting whether a piece of text contains either an electorate or a placename.

ie, Does the phrase "Melbourne" mean the suburb or the electorate?

It extracts word embeddings from a transformer model and feeds them into a Linear SVC to predict the meaning of the phrase within a sentence.

The model appears to attain 80% accuracy under K-fold validation with 0.07 stddev. It achieves nearly 100% accuracy on the training set, but that's probobaly to be expected.

## Demo

To use the demo application run:

```
# Useful to cut down on warnings
export TOKENIZERS_PARALLELISM=true
./train.py --input train.json --output serjeant.model
./predict.py --classifier serjeant.model
```

## Usage

The core routine for client code is `predicted_electorates` within `predict.py`.

## Caveats

* It's likely there are a number of edge cases in the code and it hasn't gone under anything approaching enough testing.
* The accuracy was computed algorithmically and manually checked by eyeballing a handful of examples.
* The startup time is fairly high. This is an unavoidable limitation of the underlying libraries.
* The models start from ~80MB. This will be transparently downloaded, and needs to be stored in RAM and on disk across runs.

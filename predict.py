#!/usr/bin/env python3
"""
A demo application that reads lines of text from stdin and prints the
electorates it thinks are mentioned.
"""

from typing import Iterable

import numpy as np

import re

from data import Document, load_document, token_to_word_embedding
from sentence_transformers import SentenceTransformer

WORD_RE = re.compile(r'\W+')
SENTENCE_RE = re.compile(r'[.!?]')

ALL_ELECTORATES = (
    "Adelaide", "Aston", "Ballarat", "Banks", "Barker", "Barton", "Bass",
    "Bean", "Bendigo", "Bennelong", "Berowra", "Blair", "Blaxland", "Bonner",
    "Boothby", "Bowman", "Braddon", "Bradfield", "Brand", "Brisbane", "Bruce",
    "Burt", "Calare", "Calwell", "Canberra", "Canning", "Capricornia", "Casey",
    "Chifley", "Chisholm", "Clark", "Cook", "Cooper", "Corangamite", "Corio",
    "Cowan", "Cowper", "Cunningham", "Curtin", "Dawson", "Deakin", "Dickson",
    "Dobell", "Dunkley", "Durack", "Eden-Monaro", "Fadden", "Fairfax",
    "Farrer", "Fenner", "Fisher", "Flinders", "Flynn", "Forde", "Forrest",
    "Fowler", "Franklin", "Fraser", "Fremantle", "Gellibrand", "Gilmore",
    "Gippsland", "Goldstein", "Gorton", "Grayndler", "Greenway", "Grey",
    "Griffith", "Groom", "Hasluck", "Hawke", "Herbert", "Higgins",
    "Hindmarsh", "Hinkler", "Holt", "Hotham", "Hughes", "Hume", "Hunter",
    "Indi", "Isaacs", "Jagajaga", "Kennedy", "Kingsford Smith", "Kingston",
    "Kooyong", "Lalor", "La Trobe", "Leichhardt", "Lilley", "Lindsay",
    "Lingiari", "Longman", "Lyne", "Lyons", "Macarthur", "Mackellar",
    "Macnamara", "Macquarie", "Makin", "Mallee", "Maranoa", "Maribyrnong",
    "Mayo", "McEwen", "McMahon", "McPherson", "Melbourne", "Menzies",
    "Mitchell", "Monash", "Moncrieff", "Moore", "Moreton", "Newcastle",
    "New England", "Nicholls", "North Sydney", "O'Connor", "Oxley", "Page",
    "Parkes", "Parramatta", "Paterson", "Pearce", "Perth", "Petrie", "Rankin",
    "Reid", "Richmond", "Riverina", "Robertson", "Ryan", "Scullin",
    "Shortland", "Solomon", "Spence", "Sturt", "Swan", "Sydney", "Tangney",
    "Wannon", "Warringah", "Watson", "Wentworth", "Werriwa", "Whitlam",
    "Wide Bay", "Wills", "Wright",
)


def find_electorates(body: str) -> Iterable[str]:
    """
    Yield every single electorate that is mentioned in the text. Regardless
    of predicted utility.

    This is guaranteed to avoid duplicates.

    A multi-pass algorithm is used to avoid printing electorates which are
    suffixes of other electorates. ie, the "North Sydney" and "Sydney" problem.
    """

    # Count how many times each electorate is mentioned.
    # Ignore suffix constraints.
    match_counts = {}
    for electorate in ALL_ELECTORATES:
        count = len(re.findall(r"\b%s\b" % electorate, body))
        if count > 0:
            match_counts[electorate] = count

    # Substract the number of times each electorate was a suffix match of
    # another discovered electorate. The remaining count is true match count.
    deduped_matches = set()
    for electorate, count in match_counts.items():
        suffix_count = sum(
            v for k, v in match_counts.items()
            if k != electorate and k.endswith(electorate)
        )
        assert suffix_count <= count

        if count == suffix_count:
            continue

        deduped_matches.add(electorate)

    yield from deduped_matches


def predicted_electorates(model, tf: SentenceTransformer, body: str):
    """
    Yield each electorate that is discovered in the text, and which is
    predicted to be a useful match.

    This is guaranteed to avoid duplicates.
    """
    seen = set()

    # Break the input up into sentences so they'll fit into the transformer,
    # and locate each of the mentioned electorates.
    #
    # NOTE: combining adjacent sentences could provide a little more context
    #   and hence a little more accuracy.
    # NOTE: nothing here checks if the setence is too large for the
    #   transformer. I assume that truncation is enabled rather than simply
    #   throwing exceptions.
    for sentence in filter(None, SENTENCE_RE.split(body)):
        sentence = load_document(tf, sentence)
        for electorate in find_electorates(sentence.text):
            if electorate in seen:
                continue

            # Do the actual prediction for each electorate, one at a time.
            for embedding in token_to_word_embedding(sentence, electorate, tf.tokenizer):
                embedding = np.array([embedding.tolist()])
                if any(model.predict(embedding)):
                    seen.add(electorate)
                    break

    yield from seen


if __name__ == '__main__':
    import argparse
    import pickle

    def main():
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--classifier',
            type=str,
            required=True,
            help="The path to the classifier output by the training script"
        )
        parser.add_argument(
            '--model',
            type=str,
            default='all-MiniLM-L6-v2',
            help="The transform model to use for embeddings. This MUST match the model the classifier was trained on"
        )
        args = parser.parse_args()

        with open(args.classifier, 'rb') as f:
            clf = pickle.load(f)

        tf = SentenceTransformer(args.model)

        while True:
            line = input("> enter a test comment\n")
            if not line:
                break
            print(list(predicted_electorates(clf, tf, line)))

    main()
